#ifndef CLASS_POINT_H
#define CLASSI_POINTH

/**********************
Description: CREATE CLASS POINT BASE ON STRUCT THAT INCLUDES:
PARAM :X,Y('PRIVATE')
FUNCTION POINTERS ('PUBLIC')
'THIS' POINTER TO 'SELF' STRUCT     

*/

typedef struct point point;



typedef int	(*getFunc)(point* this);
typedef void (*setFunc)(point* this , double n);


/*CREATE POINT CLASS */

point* ctor (double x, double y, getFunc* GetX,setFunc* SetX,getFunc* GetY,setFunc* SetY);

void dtor (point* this);

int GetX(point* this);

void SetX(point* this,double x);

int GetY(point* this);

void SetY(point* this,double y);

double distance(point*this, point* that);

double distanceFromZero(point *this);



#endif



