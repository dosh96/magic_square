#ifndef CLASS_CIRCLE_H
#define CLASSI_CIRCLE_H



/**********************
Description: CREATE  CLASS CIRCLE BASE ON STRUCT THAT INCLUDES:
PARAM : RADIUS('PRIVATE')
CLASS POINT (CENTER)
FUNCTION POINTERS ('PUBLIC')
'THIS' POINTER TO 'SELF' STRUCT     

*/

typedef struct Circle Circle;

typedef int	(*getFuncRadius)(Circle* this);
typedef void (*setFuncRadius)(Circle* this , double n);


/*CREATE CIRCLE CLASS */

Circle* ctorCIrcle(point* center, double radius,getFuncRadius* GetR,setFuncRadius* SetR);

void dtorCircle (Circle* this);

int GetR(Circle* this);

void SetR(Circle* this,double r);

double Circumference(Circle * this);

double Area(Circle * this);



#endif



