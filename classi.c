#include "class_point.h"
#include "class_circle.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


struct point{
  double x;
  double y;
  getFunc* gx ;
  setFunc* sx;
  getFunc* gy;
  setFunc* sy;
  point* this;    
};

struct Circle{
  point* center;
  double radius;
  getFuncRadius* gr ;
  setFuncRadius* sr;
  Circle* this;    
};


point* ctor (double x, double y, getFunc* GetX,setFunc* SetX,getFunc* GetY,setFunc* SetY)
{
	point* ip;

	if ((ip=(point*)malloc(sizeof(point)))==NULL)
		{
				printf("out of memory\n");
				return NULL;
		}
	ip->x=x;
	ip->y=y;
	ip->gx=GetX;
	ip->gy=GetY;
	ip->sx=SetX;
	ip->sy=SetY;

	return ip;
}

void dtor (point* this)
{
	free(this);
}


int GetX(point* this)
{
	return this->x; 
}

void SetX(point* this,double x)
{
	this->x=x;
}

int GetY(point* this)
{
	return this->y;
}

void SetY(point* this,double y)
{
	this->y=y;
}

double distance(point*this, point* that)
{
	return sqrt(pow((this->x)-(that->x),2)+pow((this->y)-(that->y),2));

}

double distanceFromZero(point *this)
{
	point* newPoint;
	double l;
	if ((newPoint=ctor (0,0,NULL,NULL,NULL,NULL))==NULL)
	{
		printf("allocation failed\n");
		return 0;
	}

	l=distance(this, newPoint);
	dtor (newPoint);
	return l;
}

 

Circle* ctorCIrcle(point* center, double radius,getFuncRadius* GetR,setFuncRadius* SetR)
{
	Circle* ip;

	if ((ip=(Circle*)malloc(sizeof(Circle)))==NULL)
		{
				printf("out of memory\n");
				return NULL;
		}
	ip->radius=radius;
	ip->gr=GetR;
	ip->sr=SetR;
	ip->center=center;

	return ip;
}	

void dtorCircle (Circle* this)
{
	free(this);
}

int GetR(Circle* this)
{
	return this->radius;
}

void SetR(Circle* this,double r)
{
	this->radius=r;
}




double Circumference(Circle * this)
{
	return 2*3.14*(this->radius);
}

double Area(Circle * this)
{
	return 3.14*pow((this->radius),2);
}

