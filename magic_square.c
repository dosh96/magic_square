#include <stdio.h>
#include "magic_square.h"

#define False 0
#define True  1


int check_magic(int ms[][3],int n );


/*+++++++++++++++++++++++++++++*/

static int check_row(int ms[][3] ,int n,int* sum2 )
{
	int line,row,sum1=0;
	for (row=0;row < n  && sum1==*sum2 ;++row)
	{	
		for (line=0,sum1=0;line < n;++line)
		{
			sum1+=ms[row][line];
		}
		if (row==0)
		{
			*sum2=sum1;
		}
		if (sum1!=*sum2)
		{
			return False;
		}
	}
	return *sum2;
}

static int check_line(int ms[][3] ,int n,int const sum2 )
{
	int line,row,sum1=0;
	for (line=0;line < n  && sum1==sum2 ;++line)
	{	
		for (row=0,sum1=0;row < n;++row)
		{
			sum1+=ms[row][line];
		}
		
		if (sum1!=sum2)
		{
			return False;
		}
	}
	return sum2;
}

static int check_cross1(int ms[][3] ,int n,int const sum2 )
{
	int line,row,sum1=0;
	for (row=n-1,line=n-1,sum1=0;row >=0 ;--row,--line)
	{	
		sum1+=ms[row][line];
	}
	
	if (sum1!=sum2)
	{
		return False;
	}
	return sum2;
}

static int check_cross2(int ms[][3] ,int n,int const sum2 )
{
	int line,row,sum1=0;
	for (row=0,line=0,sum1=0;row <n  ;++row,++line)
	{	
		sum1+=ms[row][line];
	}
	
	if (sum1!=sum2)
		{
			return False;
		}
	return sum2;
}





int check_magic(int ms[][3] ,int n )
{
	int sum=0;
	
	if (!check_row(ms,n,&sum))
	{
		return False;
	}	
	if (!check_line(ms,n,sum))
	{
		return False;
	}	
	if (!check_cross1(ms,n,sum))
	{
		return False;
	}	
	if (!check_cross2(ms,n,sum))
	{
		return False;
	}

	return True;
}



